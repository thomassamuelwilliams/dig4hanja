package com.eveningbread.dig4hanja;

import com.eveningbread.dig4hanja.dto.HanjaListResponse;
import com.eveningbread.dig4hanja.model.Hanja;
import com.eveningbread.dig4hanja.service.HanjaService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class Dig4hanjaApplicationTests {

    @Autowired
    private HanjaService hanjaService;

    @Test
    void contextLoads() {
        final HanjaListResponse response = hanjaService.list(0, 10);
        final Page<Hanja> hanjaList = response.getHanjaList();

        Assertions.assertTrue(response.isResult());
        Assertions.assertNotNull(hanjaList);
        Assertions.assertNotEquals(hanjaList.getSize(), 0);
    }

}
