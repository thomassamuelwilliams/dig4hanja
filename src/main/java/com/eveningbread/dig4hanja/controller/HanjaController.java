package com.eveningbread.dig4hanja.controller;

import com.eveningbread.dig4hanja.dto.HanjaListResponse;
import com.eveningbread.dig4hanja.service.HanjaService;
import org.springframework.web.bind.annotation.*;

/**
 * @author tom
 */
@CrossOrigin(origins="*")
@RestController
@RequestMapping("/hanja")
public class HanjaController {

    private final HanjaService hanjaService;

    public HanjaController(HanjaService hanjaService) {
        this.hanjaService = hanjaService;
    }

    @GetMapping("/list")
    public HanjaListResponse getHanjaList(
            @RequestParam int page,
            @RequestParam int size) {
        return hanjaService.list(page, size);
    }

}
