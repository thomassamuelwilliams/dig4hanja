package com.eveningbread.dig4hanja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Dig4hanjaApplication {

    public static void main(String[] args) {
        SpringApplication.run(Dig4hanjaApplication.class, args);
    }

}
