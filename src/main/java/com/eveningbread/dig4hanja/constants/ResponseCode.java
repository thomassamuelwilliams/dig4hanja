package com.eveningbread.dig4hanja.constants;

/**
 * @author tom
 */
public class ResponseCode {

    public static final int UNSPECIFIED_CODE = 0;
    public static final int SUCCESS = 1;

    // Bad requests
    public static final int BAD_REQUEST = 400;
    public static final int FILE_PATH_ERROR = 4000;

    // Server Error
    public static final int INTERNAL_SERVER_ERROR = 500;

}
