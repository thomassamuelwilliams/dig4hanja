package com.eveningbread.dig4hanja.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author tom
 */
@Data
@Entity
public class Hanja {

    @Id
    private Long uid;
    private String hangul;
    private String hanja;
    private String english;
    private String hiddenIndex;

}
