package com.eveningbread.dig4hanja.service;

import com.eveningbread.dig4hanja.dto.HanjaListResponse;
import com.eveningbread.dig4hanja.model.Hanja;
import com.eveningbread.dig4hanja.repository.HanjaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * @author tom
 */
@Service
public class HanjaService {

    @Autowired
    private HanjaRepository hanjaRepository;

    public HanjaListResponse list(int page, int size) {
        final HanjaListResponse response = new HanjaListResponse();

        final Sort sort = Sort.by("english");
        final PageRequest pageRequest = PageRequest.of(page, size, sort);

        final Page<Hanja> list = hanjaRepository.findAll(pageRequest);

        response.setHanjaList(list);
        response.setResultTrue();
        return response;
    }

}
