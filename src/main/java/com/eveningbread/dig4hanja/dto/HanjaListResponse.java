package com.eveningbread.dig4hanja.dto;

import com.eveningbread.dig4hanja.model.Hanja;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

/**
 * @author tom
 */
@Getter
@Setter
public class HanjaListResponse extends BaseResponse {

    Page<Hanja> hanjaList;

}
