package com.eveningbread.dig4hanja.dto;

import com.eveningbread.dig4hanja.constants.ResponseCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author tom
 */
@Data
@Builder
@AllArgsConstructor
public class BaseResponse {

    private String message;
    private boolean result;
    private int code;

    public BaseResponse() {
        this.message = "";
        this.result = false;
        this.code = ResponseCode.UNSPECIFIED_CODE;
    }

    public BaseResponse(String message, int code) {
        this(message, false, code);
    }

    protected BaseResponse(BaseResponse baseResponse) {
        this(baseResponse.getMessage(), baseResponse.isResult(), baseResponse.getCode());
    }

    public void setResultTrue() {
        this.result = true;
    }

}
