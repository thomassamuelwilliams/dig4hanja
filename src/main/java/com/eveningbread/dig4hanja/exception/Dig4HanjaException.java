package com.eveningbread.dig4hanja.exception;

import com.eveningbread.dig4hanja.constants.ResponseCode;

/**
 * @author tom
 */
public class Dig4HanjaException extends RuntimeException {

    private static final String DEFAULT_MESSAGE = "An error occurred.";
    private static final int DEFAULT_CODE = ResponseCode.INTERNAL_SERVER_ERROR;

    private final int code;

    public Dig4HanjaException(String message) {
        this(message, null);
    }

    public Dig4HanjaException(String message, Throwable cause) {
        this(message, cause, DEFAULT_CODE);
    }

    public Dig4HanjaException(String message, Throwable cause, int code) {
        super(message, cause);
        this.code = code;
    }
}
