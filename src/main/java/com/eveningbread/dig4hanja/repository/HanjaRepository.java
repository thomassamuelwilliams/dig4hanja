package com.eveningbread.dig4hanja.repository;

import com.eveningbread.dig4hanja.model.Hanja;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author tom
 */
@Repository
public interface HanjaRepository extends JpaRepository<Hanja, String> {



}
